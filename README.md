# vagrant-ansible----symfony_demo

# About
This project allow to start the symfony-demo project using Vagrant and Ansible<br>
It install also PHing and PHP_CodeSniffer


# Prerequisites
- VirtualBox (https://www.virtualbox.org/)
- Vagrant (http://www.vagrantup.com/)
- Vagrant Plugin Landrush (https://github.com/vagrant-landrush/landrush)

# Supported OS

I make this project  on Windows 10 and it works, so it should be works on Linux or Mac OS ?

# Usage
Launch a command line with elevated privileges and install the project by cloning  or download the zip file:
```sh
$ cd your-symfony-project
$ git clone https://gitlab.com/snap972/vagrant-ansible----symfony_demo.git symfony-demo
```
After the project is added, you can start the environment like this:
```sh
$ cd symfony-demo
$ vagrant up
```
 When the VM is done setting up, point your browser towards http://symfony-demo.local or http://127.0.0.1:8080

# Configuration
```sh
HostName symfony-demo.local
User vagrant
Ip 192.168.80.4
22 (guest) => 2222 (host)
80 (guest) => 8080 (host)

```

# TroubleShooting

On Windows 10, I had a lot of problems with Landrush.<br>
First, landrush could not start ‘Wired AutoConfig’ service<br>
To solve this problem, I modified “<i>configure_visibility_on_host.rb<i>” present in
```
%userprofile%\.vagrant.d\gems\2.4.4\gems\landrush-1.2.0\lib\landrush\cap\host\windows
```
by replace these two lines
          
```
 -   cmd_out = `net start`
 -   cmd_out =~ /Wired AutoConfig/m
 +   cmd_out = `sc query dot3svc`
 +   cmd_out =~ /\s*STATE\s+:\s+4\s+RUNNING/m
```

Then, landrush was unable to determine network interface. <br>
So the only solution that I found is to fix DNS manually.<br>
Launch Powershell with elevated privileges and execute netsh command to determine your Vbox interface
```
netsh interface ip show config
```
and add dns to your vbox interface
```
netsh interface ipv4 add dnsserver "VirtualBox Host-Only Network #4" address=127.0.0.1 index=1
````
